const express = require('express');
const dotenv = require('dotenv');
dotenv.config({ path: './env/.env'});
const dbConnection = require('./database/db');

class Server {
    constructor(){
        this.app = express.Router();
        this.router = express.Router();
        this.port = process.env.PORT;
        this.paths= {
            product:'/api/product',
        }
        this.middlewares();
        this.routes();
        //    localhost:3000/api/student
        this.router.use('/', this.app);
        this._express=  express().use(this.router)
    }
    middlewares(){
        this.app.use(express.json());
        
    }
    routes(){
        this.app.use(this.paths.product, require('./routes/product') )

    }
    listen(){
        this._express.listen(this.port, ()=>{
            console.log(`Servidor corriendo en puerto ${this.port}`);
        })
    }
}


module.exports = Server;