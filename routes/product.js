const Router = require('express');
const router = Router();


const product = require('../example/product.json');


const connection = require('../database/db');


router.get("/send", (req, res) => {
    try {
        product.forEach(element => {
            if(element.id == "" || element.codigo == "" || element.descripcion == "" || element.unidad == "" || element.cantidad == "" || element.proveedor == ""){
                return false;
            } else{
                const id = element.id;
                const codigo = element.codigo;
                const descripcion = element.descripcion;
                const unidad = element.unidad;
                const cantidad = element.cantidad;
                const proveedor = element.proveedor;
                connection.query(
                    'INSERT INTO product SET ?', {id: id, codigo: codigo, descripcion: descripcion, unidad: unidad, cantidad: cantidad, proveedor: proveedor}, async (error, result) => {
                        if (error) {
                            console.log("ERRROR:", error);
                        }else {
                        }
                    }
                );
            }
            
            
        });
        res.json({
            message: 'Products Data finished successfully',
            data: product
        });
    } catch (error) {
        console.log(error);
    }
})
module.exports = router;