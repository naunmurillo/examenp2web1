CREATE TABLE `product` (
    `id` int(11) NOT NULL,
    `codigo` varchar(15) NOT NULL,
    `descripcion` varchar(50) NOT NULL,
    `unidad` varchar(50) NOT NULL,
    `cantidad` varchar(50) NOT NULL,
    `proveedor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indices de la tabla `productos`
--
ALTER TABLE `product`
    ADD PRIMARY KEY (`id`);